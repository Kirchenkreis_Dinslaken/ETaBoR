# ETaBoR

Ein **E**vangelische **T**ermine **B**ootstrap **R**enderer.

Die hier entwickelten Tools lesen Termine von der [JSON API von evangelische-termine.de](https://handbuch.evangelische-termine.de/anzeige-im-internet) aus und zeigen sie mit Hilfe von [Bootstrap 5](https://getbootstrap.com/) für beliebige Webbrowser an.

## Zweck

Die zwei HTML-Dateien mit "Webapps" wurden geschrieben, um die füllende Darstellung von Terminen und Raumbelegung (Buchung von Ressourcen) auf Bildschirmen zu ermöglichen (Digital-Signage-Lösung mit Wordpress und dem Plugin [Foyer](https://codeberg.org/Kirchenkreis_Dinslaken/wp-foyer)).

Deswegen werden gerade laufende Termine auch farblich hervorgehoben.

## Nutzung

Die Darstellung ist für übliche Fernseher/Bildschirme (Full-HD oder 4K-Auflösung), ein- oder zweispaltig im Querformat optimiert.

Wer das so nutzen möchte, kopiert die html-Dateien sowie die css- und js-Verzeichnisse auf einen Webserver. In den Config-Dateien sind die URLs mit den Parametern entsprechend der [Dokumentaton von evangelische-termine.de](https://handbuch.evangelische-termine.de/anzeige-im-internet/ausgabe-parameter) anzupassen.

Dabei ist zu beachten, dass die JSON API leider nicht stateless ist. D.h. einmal vorgenommene Parameter werden für den nächsten Aufruf beibehalten, wenn sie nicht überschrieben werden.

Wer einfach Termine anzeigen möchte, verwendet die `*events*` genannten Dateien.

Für die Ressourcen (Räume etc.) sind die `*resources*` genannten Dateien zu verwenden. Die Ressourcen können anscheinend nur über die [SOAP-API](https://handbuch.evangelische-termine.de/import/soap-api-dokumentation#resource) abgefragt werden. D.h. die Angaben zu den auszuwertenden Ressourcen müssen manuell bereit gestellt werden wie in der Datei `config-resources.js` zu sehen.

## Bearbeitung / Entwicklung

Wer die Ausgabe anpassen möchte, wird die Bootstrap-Quelldateien verwenden und die `scss/styles.scss`-Datei bearbeiten wollen. Die CSS-Datei wird dann mit Hilfe von [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) neu generiert.

```
npm install
npm run css
```

Danach findet sich eine frische CSS-Datei im Verzeichnis `css`.

Für eine kleinere Anzeige einfach eine kleinere `font-size` verwenden.


## Copyright / Lizenz

&copy; 2022 Johannes Brakensiek, [eMail](mailto:codeberg@codingpastor.de).

Nutzung, Änderung und Weitergabe nur unter den Bedingungen der [AGPL 3.0 oder später](https://www.gnu.org/licenses/agpl-3.0.de.html).

