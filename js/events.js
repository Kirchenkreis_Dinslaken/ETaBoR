async function getEvents(url) {
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}

async function renderEvents(events) {
    let html = '';

    let loadingSpinner = document.querySelector(`.loading-spinner`);
    loadingSpinner.classList.toggle('d-none');

    events.forEach(event => {
        let startingTime = new Date(event.Veranstaltung.START_RFC);
        let endingTime = new Date(event.Veranstaltung.END_RFC);
        let nowTime = new Date();

        let activeString = '';
        if (nowTime >= startingTime && nowTime <= endingTime)
            activeString = ' active';

        let personString = '';
        if (event.Veranstaltung._event_TEXTLINE_1 != '')
            personString = `– mit ${event.Veranstaltung._event_TEXTLINE_1}`;

        let htmlSegment = `<a href="#" class="list-group-item list-group-item-info list-group-item-action${activeString}" aria-current="true">
        <div class="d-flex w-100 justify-content-between">
          ${event.Veranstaltung.DATUM}
          <h5 class="mb-1">${event.Veranstaltung._event_TITLE}</h5>
        </div>
        <p class="mb-1">${event.Veranstaltung._event_SHORT_DESCRIPTION}</p>
        <small>${event.Veranstaltung._place_NAME} ${personString}</small>
        </a>`;

        html += htmlSegment;
    });

    if (events.length == 0) {

        html = `<div class="alert alert-info" role="alert">
            Hier sind aktuell keine Termine eingetragen.
        </div>`;

        let container = document.querySelector(`.col`);
        container.insertAdjacentHTML('beforeend', html);

    } else {

        let container = document.querySelector(`.events-schedule`);
        container.innerHTML = html;

    }
}

function renderCategory() {
    let htmlSegment = `      <div class="col-md-7">
        <h2 class="px-5 p-2">${eventName} der nächsten Zeit:</h2>
        <div class="list-group events-schedule p-2 px-4 text-end">
        </div>
        <div class="d-flex justify-content-center loading-spinner">
            <div class="spinner-border" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>
      </div>`;

    let container = document.querySelector('.row');
    container.insertAdjacentHTML('beforeend', htmlSegment);

    getEvents(urlConst).then((events) => renderEvents(events))
        .catch(error => console.log(error));
}

renderCategory();
