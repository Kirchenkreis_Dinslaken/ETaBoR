async function getEvents(url) {
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}

async function renderEvents(events, resourceIdentifier) {
    let html = '';

    let loadingSpinner = document.querySelector(`.loading-spinner-${resourceIdentifier}`);
    loadingSpinner.classList.toggle('d-none');

    events.forEach(event => {
        let endingTimeString = '';
        if (event.Veranstaltung.END_UHRZEIT != '')
            endingTimeString = ` - ${event.Veranstaltung.END_UHRZEIT} Uhr`;

        let startingTime = new Date(event.Veranstaltung.START_RFC);
        let endingTime = new Date(event.Veranstaltung.END_RFC);
        let nowTime = new Date();

        let activeString = '';
        if (nowTime >= startingTime && nowTime <= endingTime)
            activeString = ' active';

        let htmlSegment = `<a href="#" class="list-group-item list-group-item-action${activeString}" aria-current="true">
        <div class="d-flex w-100 justify-content-between">
          ${event.Veranstaltung.START_UHRZEIT} Uhr${endingTimeString}
          <h5 class="mb-1">${event.Veranstaltung._event_TITLE}</h5>
        </div>
        <p class="mb-1">${event.Veranstaltung._event_SHORT_DESCRIPTION}</p>
        <small>${event.Veranstaltung._person_NAME}</small>
        </a>`;

        html += htmlSegment;
    });

    if (events.length == 0) {

        html = `<div class="alert alert-info" role="alert">
            Hier sind aktuell keine Termine eingetragen.
        </div>`;

        let container = document.querySelector(`.col-${resourceIdentifier}`);
        container.insertAdjacentHTML('beforeend', html);

    } else {

        let container = document.querySelector(`.resource-schedule-${resourceIdentifier}`);
        container.innerHTML = html;

    }
}

function renderResources(resources) {
    resources.forEach(resource => {
        let htmlSegment = `      <div class="col-md-6 col-${resource.resourceIdentifier}">
        <h2 class="px-5 p-2">Heute im ${resource.resourceName}:</h2>
        <div class="list-group resource-schedule-${resource.resourceIdentifier} p-2 px-4 text-end">
        </div>
        <div class="d-flex justify-content-center loading-spinner-${resource.resourceIdentifier}">
            <div class="spinner-border" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>
      </div>`;

        let container = document.querySelector('.row');
        container.insertAdjacentHTML('beforeend', htmlSegment);

        let url = urlConst.replace('RESOURCE_ID', resource.resourceID);
        getEvents(url).then((events) => renderEvents(events, resource.resourceIdentifier))
            .catch(error => console.log(error));
    });
}

renderResources(resourceConfig);
