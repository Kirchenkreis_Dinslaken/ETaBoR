const urlConst = 'https://termine.ekir.de/json?vid=149&res=RESOURCE_ID&today=true&past=2&dest=all';

let resourceConfig = [
    {
        resourceID: 336,
        resourceIdentifier: 'jk-godi',
        resourceName: 'Gottesdienstraum'
    },
    {
        resourceID: 337,
        resourceIdentifier: 'jk-bbz',
        resourceName: 'BBZ/Begegnungsraum'
    }
];